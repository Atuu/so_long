# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aboyreau <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/02/26 14:16:25 by aboyreau          #+#    #+#              #
#    Updated: 2024/02/26 21:44:37 by aboyreau         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = so_long

CC = cc
LIB = libft/libft.a minilibx-linux/libmlx.a
CFLAGS += -Wall -Werror -Wextra -g
LDFLAGS += -Lminilibx-linux -Iminilibx-linux -lXext -lX11 -lm -lz

FILES=so_long
SRC=$(addprefix $(FILES),.c)
OBJ=$(addprefix $(FILES),.o)

all: deps $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $(LIB) $(LDFLAGS) -o $(NAME)

%.o: %.c
	$(CC) $(CFLAGS) -I/usr/include -Imlx_linux -O3 -c $< -o $@

clean:
	$(MAKE) clean -C libft
	rm $(OBJ) -f

fclean: clean
	$(MAKE) fclean -C libft
	rm $(NAME) -f

re: fclean clean all

deps:
	$(MAKE) -C minilibx-linux
	$(MAKE) -C libft
