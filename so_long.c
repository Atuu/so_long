/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboyreau <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 14:28:55 by aboyreau          #+#    #+#             */
/*   Updated: 2024/02/26 23:30:08 by aboyreau         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "minilibx-linux/mlx.h"
#include "libft/libft.h"

struct	s_map
{
	char **textures;
	char **map;
	int	coords[2];
	int	collectibles;
	int	exit;
};

typedef struct	s_map t_map;

void	parse_map(int argc, char **argv, t_map *map);
void	check_map(t_map map);
void	run_game(t_map map);

void	parse_map(int argc, char **argv, t_map *map)
{
	int		i;
	int		j;
	int		fd;
	char	*line;

	line = NULL;
	map->map = ft_calloc(16, sizeof(char *));
	if (argc != 2)
		map->map = NULL;
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		map->map = NULL;
	if (map->map == NULL)
		return ;
	i = 0;
	while ((line = get_next_line(fd)) != NULL)
	{
		map->map[i] = line;
		i++;
		if (i % 16 == 0)
			ft_grow(map, (i - 15) * sizeof(char *), (i + 1) * sizeof(char *));
	}
	map->collectibles = 0;
	map->exit = 0;
	i = 0;
	j = 0;
	while (map->map[i])
	{
		if (map->map[i][j] == '\n' || map->map[i][j] == '\0')
		{
			i++;
			j = 0;
		}
		if (map->map[i] == NULL)
			return ;
		if (map->map[i][j] == 'C')
			map->collectibles++;
		if (map->map[i][j] == 'E')
			map->exit++;
		j++;
	}
}

void	free_map(t_map map)
{
	int	i;

	i = 0;
	while (map.map[i])
		free(map.map[i++]);
	free(map.map);
	if (map.textures)
	{
		free(map.textures['0']);
		free(map.textures['1']);
		free(map.textures['C']);
		free(map.textures['E']);
		free(map.textures['P']);
		free(map.textures);
	}
}

void	check_map(t_map map)
{
	int		i;
	size_t	len;

	if (map.map == NULL)
	{
		write(2, "The map is invalid\n", 19);
		free_map(map);
		exit(EXIT_FAILURE);
	}
	i = 0;
	len = ft_strlen(map.map[i]);
	while (map.map[i])
	{
		if (ft_strlen(map.map[i]) != len)
		{
			write(2, "The map should be a square\n", 27);
			free_map(map);
			exit(EXIT_FAILURE);
		}
		len = ft_strlen(map.map[i]);
		i++;
	}
	if (map.exit != 1)
	{
		write(2, "The map should contain an exit\n", 30);
		exit(EXIT_FAILURE);
	}
	if (map.collectibles <= 0)
	{
		write(2, "The map should contains collectibles\n", 46);
		free_map(map);
		exit(EXIT_FAILURE);
	}
}

void	load_texture(void *mlx, t_map *map)
{
	int	a, b;

	map->textures = malloc('Q' * sizeof(void *));
	map->textures['0'] = mlx_xpm_file_to_image(mlx, "assets/sprites/floor.xpm", &a, &b);
	map->textures['1'] = mlx_xpm_file_to_image(mlx, "assets/sprites/wall.xpm", &a, &b);
	map->textures['C'] = mlx_xpm_file_to_image(mlx, "assets/sprites/coin-bag.xpm", &a, &b);
	map->textures['E'] = mlx_xpm_file_to_image(mlx, "assets/sprites/exit-closed.xpm", &a, &b);
	map->textures['P'] = mlx_xpm_file_to_image(mlx, "assets/sprites/player/front.xpm", &a, &b);
	if (!map->textures['0'] || !map->textures['1'] || !map->textures['C'] || !map->textures['E'] || !map->textures['P'])
	{
		write(2, "Can't load the textures\n", 24);
		free_map(*map);
		exit(EXIT_FAILURE);
	}
}

void	*create_window(void *mlx, t_map *map)
{
	int		x;
	int		y;
	void	*window;

	x = ft_strlen(map->map[0]);
	y = 0;
	while (map->map[y])
		y++;
	window = mlx_new_window(mlx, (x - 1) * 32, y * 32, "");
	return (window);
}

void	display_board(void *mlx, t_map *map, void *window)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while (map->map[x])
	{
		mlx_put_image_to_window(mlx, window, map->textures[(int) map->map[x][y]], y * 32, x * 32);
		y++;
		if (map->map[x][y] == '\n')
		{
			x++;
			y = 0;
		}
	}
}

int	mlx_close(int keycode, void *mlx_ptr)
{
	if (keycode != 65307)
		return (-1);
	mlx_loop_end(mlx_ptr);
	return (0);
}

int	mlx_close_cross(void *mlx_ptr)
{
	return (mlx_close(65307, mlx_ptr));
}

void	move_to_up(t_map *map)
{
	int	x;
	int	y;
	
	x = 0;
	y = 0;
	while (map->map[x])
	{
		if (map->map[x][y] == '\0')
		{
			x++;
			y = 0;
			continue ;
		}
		if (map->map[x][y] == 'P' && (map->map[x - 1][y] == 'C' || map->map[x - 1][y] == '0'))
		{
			if (map->map[x - 1][y] == 'C')
				map->collectibles--;
			map->map[x][y] = '0';
			map->map[x - 1][y] = 'P';
			return ;
		}
		if (map->map[x][y] == 'P' && map->map[x - 1][y] == 'E' && map->collectibles == 0)
			exit(1);
		y++;
	}
}

void	move_to_down(t_map *map)
{
	int	x;
	int	y;
	
	x = 0;
	y = 0;
	while (map->map[x])
	{
		if (map->map[x][y] == '\0')
		{
			x++;
			y = 0;
			continue ;
		}
		if (map->map[x][y] == 'P' && (map->map[x + 1][y] == 'C' || map->map[x + 1][y] == '0'))
		{
			if (map->map[x + 1][y] == 'C')
				map->collectibles--;
			map->map[x][y] = '0';
			map->map[x + 1][y] = 'P';
			return ;
		}
		if (map->map[x][y] == 'P' && map->map[x + 1][y] == 'E' && map->collectibles == 0)
			exit(1);
		y++;
	}
}

void	move_to_left(t_map *map)
{
	int	x;
	int	y;
	
	x = 0;
	y = 0;
	while (map->map[x])
	{
		if (map->map[x][y] == '\0')
		{
			x++;
			y = 0;
			continue ;
		}
		if (map->map[x][y] == 'P' && (map->map[x][y - 1] == 'C' || map->map[x][y - 1] == '0'))
		{
			if (map->map[x][y - 1] == 'C')
				map->collectibles--;
			map->map[x][y] = '0';
			map->map[x][y - 1] = 'P';
			return ;
		}
		if (map->map[x][y] == 'P' && map->map[x][y - 1] == 'E' && map->collectibles == 0)
			exit(1);
		y++;
	}
}

void	move_to_right(t_map *map)
{
	int	x;
	int	y;
	
	x = 0;
	y = 0;
	while (map->map[x])
	{
		if (map->map[x][y] == '\0')
		{
			x++;
			y = 0;
			continue ;
		}
		if (map->map[x][y] == 'P' && (map->map[x][y + 1] == 'C' || map->map[x][y + 1] == '0'))
		{
			if (map->map[x][y + 1] == 'C')
				map->collectibles--;
			map->map[x][y] = '0';
			map->map[x][y + 1] = 'P';
			return ;
		}
		if (map->map[x][y] == 'P' && map->map[x][y + 1] == 'E' && map->collectibles == 0)
			exit(1);
		y++;
	}
}

int	move(int keycode, void *args)
{
	void		*mlx;
	void		*window;
	t_map		*map;
	static int	i = 0;

	mlx = ((void **)args)[0];
	window = ((void **)args)[1];
	map = ((void **)args)[2];
	mlx_close(keycode, mlx);
	ft_printf("action number %d\n", ++i);
	if (keycode == 65361)
		move_to_left(map);
	if (keycode == 65362)
		move_to_up(map);
	if (keycode == 65363)
		move_to_right(map);
	if (keycode == 65364)
		move_to_down(map);
	ft_printf("%d\n", map->collectibles);
	display_board(mlx, map, window);
	return (0);
}

void	set_up_hooks(void *mlx, t_map *map, void *window)
{
	mlx_hook(window, 2, (1L<<0), move, (void*[]){mlx, window, map});
	mlx_hook(window, 33, (1L<<17), mlx_close_cross, mlx);
	(void) map;
}

int	main(int argc, char **argv)
{
	t_map	map;
	void	*mlx;
	void	*window;

	mlx = mlx_init();
	if (mlx == NULL)
	{
		write(2, "The mlx couldn't be initialized.\n", 33);
		exit(EXIT_FAILURE);
	}
	parse_map(argc, argv, &map);
	load_texture(mlx, &map);
	check_map(map);
	window = create_window(mlx, &map);
	display_board(mlx, &map, window);
	set_up_hooks(mlx, &map, window);
	mlx_loop(mlx);
	free_map(map);
	mlx_destroy_display(mlx);
	free(mlx);
	return (EXIT_SUCCESS);
}
